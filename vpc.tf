resource "aws_vpc" "cd-vpc" {
  cidr_block = "10.0.0.0/16"
  assign_generated_ipv6_cidr_block = true

  enable_dns_hostnames = true

  tags = {
    Name = "Cyber-Duck VPC"
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_subnet" "private" {
  count = 3
  vpc_id     = aws_vpc.cd-vpc.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block = "10.0.${count.index}.0/24"
  ipv6_cidr_block = cidrsubnet(aws_vpc.cd-vpc.ipv6_cidr_block, 8, count.index)
  assign_ipv6_address_on_creation = true
  enable_resource_name_dns_aaaa_record_on_launch = true

  tags = {
    Name = "Private-${count.index}"
  }
}

resource "aws_subnet" "rds" {
  count = 3
  vpc_id     = aws_vpc.cd-vpc.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block = "10.0.${count.index+3}.0/24"
  ipv6_cidr_block = cidrsubnet(aws_vpc.cd-vpc.ipv6_cidr_block, 8, count.index+3)
  assign_ipv6_address_on_creation = true
  enable_resource_name_dns_aaaa_record_on_launch = true

  tags = {
    Name = "RDS-${count.index}"
  }
}

resource "aws_db_subnet_group" "aurora" {
  name       = "aurora"
  subnet_ids = tolist(aws_subnet.rds[*].id)

  tags = {
    Name = "Aurora subnet group"
  }
}
