resource "aws_security_group" "mysql" {
  name        = "Allow MySQL"
  description = "Allow MySQL traffic"
  vpc_id      = aws_vpc.cd-vpc.id

  ingress {
    description = "MySQL ingress"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    self        = true
  }

  egress {
    description = "MySQL egress"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self        = true
  }

  tags = {
    Name = "MySQL traffic"
  }
}
