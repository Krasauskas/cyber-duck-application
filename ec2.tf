data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "webserver" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"

  vpc_security_group_ids = [aws_security_group.mysql.id]
  subnet_id = aws_subnet.private[0].id
  ipv6_address_count = 1

  key_name = "cd-task"

  user_data = <<EOF
#!/bin/bash
echo ubuntu:changeme | chpasswd

EOF

  tags = {
    Name = "webserver for cyber-duck task"
  }
}
