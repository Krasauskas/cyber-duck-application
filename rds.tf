module "cd-db" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "7.7.0"

  name           = "cd-aurora-db-mysql57"
  engine         = "aurora-mysql"
  engine_mode    = "provisioned"
  engine_version = "8.0.mysql_aurora.3.03.0"
  instance_class = "db.t3.medium"
  instances = {
    one   = {}
    two   = {}
    three = {}
  }

  vpc_id  = aws_vpc.cd-vpc.id
  subnets = aws_subnet.rds[*].id

  create_db_subnet_group = false
  db_subnet_group_name   = "aurora"
  create_security_group  = false
  create_monitoring_role = false

  allowed_security_groups = [aws_security_group.mysql.id]

  storage_encrypted          = true
  apply_immediately          = true
  auto_minor_version_upgrade = true

  autoscaling_enabled      = true
  autoscaling_max_capacity = 4
  autoscaling_min_capacity = 2

  tags = {
    Environment = "production"
    Terraform   = "true"
  }
}
